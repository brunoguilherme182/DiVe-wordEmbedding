from glove import Corpus, Glove
from gensim.models import Word2Vec
from DSWEDualPoint import DiVeWordEmbedding
import numpy as np
import scipy as sp
import scipy.stats
import fasttext

import os 

#os.environ["THEANO_FLAGS"] = "floatX=float32"
#os.environ["THEANO_FLAGS"] = 'optimizer=fast_compile, floatX=float32,exception_verbosity=high'

#from interfaces.interface_configurator import InterfaceConfigurator
#from libraries.evaluation.support import evaluate
#from libraries.evaluation.lexsub.run_lexsub import run_lexsub


def gloveData(sentences,glove,sizeEmb,window,min_count=1,test=0):

    X = []

    if test == 0:

        glove = Glove(no_components=sizeEmb)
        corpus = Corpus()
        corpus.fit(sentences, window=window)

        glove.fit(corpus.matrix)
        glove.add_dictionary(corpus.dictionary)

        for x in sentences:

            aux = np.zeros(sizeEmb)

            for word in x:

                if word in glove.dictionary:

                    aux = aux+glove.word_vectors[glove.dictionary[word]]

            X.append(aux)
    else:

        for x in sentences:

            aux = np.zeros(sizeEmb)

            for word in x:

                if word in glove.dictionary:

                    aux = aux+glove.word_vectors[glove.dictionary[word]]

            X.append(aux)

    return X,glove

def word2vecData(sentences,word2vec,sizeEmb,window,min_count=1,test=0):

    X= []

    if test == 0:

        word2vec = Word2Vec(sentences, size=sizeEmb, window=window, min_count=min_count)

        for x in sentences:

            aux = np.zeros(sizeEmb)

            for word in x:

                if word in word2vec:

                    aux = aux+word2vec[word]

            X.append(aux)
    else:

        for x in sentences:

            aux = np.zeros(sizeEmb)

            for word in x:

                if word in word2vec:

                    aux = aux+word2vec[word]

            X.append(aux)

    return X,word2vec

def DiVeData(sentences,DiVe,sizeEmb,window,dist=2,test=0):

    X= []

    if test == 0:

        DiVe = DiVeWordEmbedding(dataset=sentences,size=sizeEmb,window=window,d=dist)
        return DiVe

        for x in sentences:

            aux = np.zeros(sizeEmb)

            for word in x:

                if word in DiVe.vocabulary:

                        aux += DiVe.inVector[DiVe.vocabulary[word]]

            X.append(aux)
    else:

        for x in sentences:

            aux = np.zeros(sizeEmb)

            for word in x:

                if word in DiVe.vocabulary:

                        aux += DiVe.inVector[DiVe.vocabulary[word]]

            X.append(aux)
        
    return X,DiVe

def FastText(sentences,word2vec,sizeEmb,window,min_count=1,test=0):

    X = []

    if test == 0:

        joinData = []

        for x in sentences:
            
            aux = ""

            for word in x:

                aux += word+" "

            joinData.append(aux)


        outfile = open("data.txt","w") 
        outfile.write("\n".join(joinData))

        word2vec = fasttext.cbow("data.txt", 'model',dim=sizeEmb,ws=window)

        for x in sentences:

            aux = np.zeros(sizeEmb)

            for word in x:

                if word in word2vec:

                    aux = aux+word2vec[word]

            X.append(aux)
    else:

        for x in sentences:

            aux = np.zeros(sizeEmb)

            for word in x:

                if word in word2vec:

                    aux = aux+word2vec[word]

            X.append(aux)

    return X,word2vec

def buildBayesianSG(sentences,bg,sizeEmb,window,min_count=1,test=0):

    joinData = []
    X = []

    for x in sentences:
    
        aux = ""

        for word in x:

            aux+=word+" "

        joinData.append(aux)


    if test == 0:

        outfile = open("data.txt","w") 
        outfile.write("\n".join(joinData))

        train_data_path = 'data.txt'
        vocab_file_path = 'b.txt' 
        output_folder_path = ""  

        i_model = InterfaceConfigurator.get_interface(train_data_path, vocab_file_path, output_folder_path)

        i_model.train_workflow()

        # store the temporary vocab, because it can be different from the original one(e.g. smaller number of words)
        vocab = i_model.vocab

        temp_vocab_file_path = os.path.join(i_model.output_path, "vocab.txt")
        vocab.write(temp_vocab_file_path)

        mu_vecs = [os.path.join(i_model.output_path, "mu.vectors")]
        sigma_vecs = [os.path.join(i_model.output_path, "sigma.vectors")]

        vec = open(mu_vecs[0],"r")
        m = {}
        inVector = []
        cont = 0
        for line in vec:

                so = line.split(" ")
                m[so[0]] = cont
                aux = so[1:]
                inVector.append(aux)

        inVector = np.array(inVector).astype('float32')

        for line in sentences:

            aux = np.zeros(sizeEmb)

            for word in line:

                if word in m:

                    aux += inVector[m[word]]

            X.append(aux)

        return X,[inVector,m]

    else:

        for line in sentences:

            aux = np.zeros(sizeEmb)

            for word in line:
                
                if word in bg[1]:

                    aux += bg[0][bg[1][word]]

            X.append(aux)
        return X,[bg[0],bg[1]]
import matplotlib.pyplot as plt
if __name__ == '__main__':
    
    print "Hello World"

    files = [['custrev.npy','labels-subj.npy']]

    sizeEmb = 400
    window = 5
    min_count = 1
    path = 'loads-Datas/'
    names = ['Euclidean','Canberra','City Block']
    mm = ['o','s','v']
    cc = ['black','gray','brown']
    for f in files:

        sentences = np.load(path+f[0])
        label = np.load(path+f[1])
        for l in range(0,3):
            print l
            X = DiVeData(sentences,[],sizeEmb,window,dist=l)
            from sklearn.manifold import TSNE
            maps = {}
            for x in X.cdf:

                if x not in maps:
                    maps[x] = 1
                else:
                    maps[x] += 1
            s = 0
            a = []
            b = []
            for x in sorted(maps):
                s += maps[x]
                a.append(x)
                b.append(s/float(len(X.cdf))) 
                #print x,s/float(len(X.cdf))

            #s = TSNE(n_components=2).fit_transform(X)
            #np.save("subj",s)
            #print names[x],mm[x],cc[x]
            plt.plot(a,b,markersize=12,label=names[l], marker=mm[l],color=cc[l])
        plt.xscale('log')
        plt.grid()
        plt.legend(frameon=False,loc='upper left',numpoints=1)
        plt.xlabel("# neighbours occurrence",fontsize=18)
        plt.ylabel("CDF",fontsize=18)
        plt.show()






