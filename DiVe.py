import numpy as np
import pandas as pd
import random
import copy
import math

from scipy.spatial.distance import euclidean
from scipy.spatial.distance import cosine
from scipy.spatial.distance import chebyshev
from scipy.spatial.distance import cityblock
from scipy.spatial.distance import braycurtis
from scipy.spatial.distance import canberra

from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.neighbors import KNeighborsClassifier

from sklearn.metrics import f1_score
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score

from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score


import scipy as sp
import scipy.stats



class DiVeWordEmbedding:

    N = 0

    def __init__(self,dataset,size=400,alpha=0.001,window=5,negSampling=0,threshold=-1,eta=0,etaN=1,d=0,W=[],Importance=10,IN=0,al=0.1,gama=10):

        """
        Main Class For build word embedding DiVe
        """

        ### Vocabulary Variables
        self.N = 0
        self.vocabulary = {}
        self.vocabularyCount = {}
        self.probsChoice = []
        self.constWeight = 0

        ### Train Variables
        self.train = []
        self.alpha = alpha
        self.window = window

        self.distances = [euclidean,canberra,cityblock,euclidean,cosine]


        self.countWords(dataset)
        self.createVocabulary(dataset,window,threshold)

        ### For Importance Sampling  and Negative Sampling
        self.createProbs(eta,etaN)

        self.LearningEmbedding(alpha,window,negSampling,size,eta,d,W,Importance,IN,al,gama)

    def countWords(self,dataset):

        '''
       
        Count Occurrence of Words in DataSet . Return Total Number of words apperrence
        and total number apperrence of individual words
       
        '''
        for line in dataset:

            for word in line:

                if word not in self.vocabularyCount:

                    self.vocabularyCount[word] = 0

                self.vocabularyCount[word] += 1

        for freqWord in self.vocabularyCount:

            self.N += self.vocabularyCount[freqWord]

    def createVocabulary(self,dataset,window,threshold):

        '''
        Create the vocabulary using for train the model, if necessary discard words with low Occurrence.
        Build the train set based  n-grams(window)
        '''
        index = 0

        for line in dataset:

            sequence = []

            for word in line:

                probs = self.vocabularyCount[word]


                if probs < threshold:
                    continue

                if word not in self.vocabulary:
             
                    self.vocabulary[word] = index
                    index += 1

                sequence.append(self.vocabulary[word])

                if len(sequence) == window+1:

                  
                    sentence = []

                    for i in range(0,window+1):

                        sentence.append(sequence[i])

                    self.train.append(sentence)
                    sequence.pop(0)

    def createProbs(self,eta,etaN):

        '''
        Create a probability of appearance of a word in dataset The probability
        will be use in negative Sampling and in Importance Sampling
        '''
       
        self.N = 0
        self.probsChoice = np.zeros(len(self.vocabulary))
        self.probsChoiceN = np.zeros(len(self.vocabulary))

        for freqWord in self.vocabularyCount:

            if freqWord in self.vocabulary:

                    self.N += self.vocabularyCount[freqWord]


        for word in self.vocabulary:

            self.probsChoice[self.vocabulary[word]] = ((self.vocabularyCount[word])/float(self.N))**(eta)
            self.constWeight += 1/float(self.probsChoice[self.vocabulary[word]])
           
            self.probsChoiceN[self.vocabulary[word]] = ((self.vocabularyCount[word])/float(self.N))**(etaN)
       
        norm = np.sum(self.probsChoice)
        self.probsChoice = self.probsChoice/float(norm)
        self.constWeight = self.constWeight**(-1)

    def constZ(self,mvec,vocabulary,d):


        vecZ = []
        probs = []
        W = 0
        for word in vocabulary:

            dist = -(self.distances[d](self.outVector[word],mvec))
          
            vecZ.append(max(np.exp(dist),0.000000000000001)) # prevent underflow
            probs.append(self.probsChoice[word])
            W += 1/self.probsChoice[word]

        return np.sum(vecZ),vecZ,probs,W

    def constZIN(self,mvec,d):


        dist = 0

        vecZ = []
        probs = []
       
        for w in self.vocabulary:
              
                dist = 0
                word = self.vocabulary[w]
                dist -= self.distances[d](self.outVector[word],mvec)
              
                vecZ.append(max(np.exp(dist),0.000000000000001)) # prevent underflow
                probs.append(self.probsChoice[word])

        return np.sum(vecZ),vecZ,probs

    # Derivatives Distances
    def gradient(self,X,Y,d,context=-1,word=-1,divide=1.0):

        '''
        Calculate the first derivative of context and word with respect a some distance
        '''

        ### Euclidean Distance

        if d == 0:

            context = (X-Y)/self.distances[d](X,Y)*divide
            word = -(X-Y)/self.distances[d](X,Y)

            return context,word

        ### Canberra Distance

        if d == 1:

            context = (X-Y)/(divide*(np.abs(X)+np.abs(Y))*np.abs(X-Y)) -  (divide*X*np.abs(X-Y))/(divide*(np.abs(X)+np.abs(Y))**2*np.abs(divide*X))

            word = (Y-X)/((np.abs(X)+np.abs(Y))*np.abs(Y-X)) - (Y*np.abs(Y-X))/(np.abs(Y)*(np.abs(Y)+np.abs(X))**2)

            return context,word

        ### City Block Distance
        if d == 2:

            context = (X-Y)/(np.abs(X-Y))*divide
            word = (Y-X)/np.abs(X-Y)

            return  context,word
       
        #Need to build
        if d == 3:

            #print self.distances[d](X,Y),'dist'
            context = (X-Y)
            word = (Y-X)

            return  context,word

    #Derivative Importance Sampling

    def gradientIM(self,mvec,vocabulary,Z,vecZ,probs,divide,d,W):

        '''
        Calculate the first derivative of context and word with respect a some distance
        '''
        ### Euclidean Distance

        if d == 0:


            top = 0
            down = 0
           
            keepWords = [ np.zeros(len(mvec)) for x in range(0,len(vocabulary))]

            for index in range(0,len(vocabulary)):
               
                word = vocabulary[index]
                top -= W*((mvec-self.outVector[word])*vecZ[index])/(divide*probs[index]*self.distances[d](mvec,self.outVector[word]))
                down +=  W*vecZ[index]/probs[index]

                keepWords[index] += ((mvec-self.outVector[word])*vecZ[index])/(probs[index]*self.distances[d](mvec,self.outVector[word]))

            return top/down,keepWords/down



        if d == 1:

            top = 0
            down = 0

            keepWords = [ np.zeros(len(mvec)) for x in range(0,len(vocabulary))]

            for index in range(0,len(vocabulary)):
               
                word = vocabulary[index]
                Y = self.outVector[word]

                top -= (W*np.exp(vecZ[index]/probs[index])*(mvec-Y)/(divide*(np.abs(mvec)+np.abs(Y))*np.abs(mvec-Y)) - (divide*mvec*np.abs(mvec-Y))/(divide*(np.abs(mvec)+np.abs(Y))**2*np.abs(divide*mvec)))/probs[index]
                down += W*np.exp(vecZ[index]/probs[index])
               
                keepWords[index] += (W*np.exp(vecZ[index]/probs[index])*(mvec-Y)*vecZ[index])/(probs[index]*self.distances[d](mvec,Y))

            return top/down,keepWords/(down*np.array(probs))

        if d == 2:

            top = 0
            down = 0

            keepWords = [ np.zeros(len(mvec)) for x in range(0,len(vocabulary))]

            for index in range(0,len(vocabulary)):
               
                word = vocabulary[index]
                Y = self.outVector[word]

                top -= (W*(mvec-Y)*np.exp(vecZ[index]/probs[index]))/(divide*probs[index]*np.abs(mvec-Y))
                down += W*np.exp(vecZ[index]/probs[index])
               
                keepWords[index] += (W*np.exp(vecZ[index]/probs[index])*(mvec-Y))/(Y-mvec)
            #print np.array(probs).T.shape
            #print np.array(keepWords).shape
            return top/down,np.array(keepWords)/(down*np.array(probs).reshape(10,1))
       
        #Need to build
        if d == 3:

            pass
            return None

    # Derivative of Infrequent Normalization 
    def gradientIN(self,mvecx,vocabularyC,divide,d,alpha,gama):

        '''
        Calculate the first derivative of context and word with respect a some distance
        '''
        ### Euclidean Distance

        if d == 0:


            top = 0
            down = 0

            keepWords = [ np.zeros(len(mvecx)) for x in range(0,len(self.vocabulary))]
            keepC = [ np.zeros(len(mvecx)) for x in range(0,len(vocabularyC))]

            cont = 0
           
            for mvec in vocabularyC:

                Z,vecZ,probs = self.constZ2(mvec,d)
                topC = 0
                for index in range(0,len(self.vocabulary)):
                   
                    word = index
                    keepWords[index] += 2*(alpha/gama)*((mvec-self.outVector[word])*vecZ[index]*np.log(sum(vecZ)))/(vecZ[index]*sum(vecZ))
                    topC += (2*(alpha/gama)*(-(mvec-self.outVector[word])*vecZ[index]*np.log(sum(vecZ)))/(vecZ[index]*divide))/sum(vecZ)

                keepC[cont] = topC
                cont+=1

            return keepC,keepWords
           
        ### Need Build
        if d == 1:
            pass
            return None

        ### Need Build
        if d == 2:

            pass
            return  None
       
        #Need to build
        if d == 3:

            pass
            return None


    def LearningEmbedding(self,alpha,window,negSampling,size,eta,d,W,Importance,IN,al,gama):

        '''
        Learning Dive Embedding
        '''


        randC = len(self.train)
        randV = len(self.vocabulary)

        self.alpha = alpha

        self.outVector = np.zeros([len(self.vocabulary),size])

        if len(W) == 0:

            if d == 2:

                self.inVector = np.random.uniform(low=-1,high=1,size=(len(self.vocabulary),size))

            if d == 1:

                self.inVector = np.random.uniform(low=5,high=7,size=(len(self.vocabulary),size))
                self.outVector = self.inVector
               
            if d == 0 or d == 3:

                self.inVector = np.random.uniform(low=-0.5,high=0.5,size=(len(self.vocabulary),size))
        else:

            self.inVector = W

        for i in range(0,len(self.train)):

            c = i

            im = np.random.choice(randV,Importance,p=self.probsChoice)
           
            negs = np.random.choice(randV,negSampling,p=self.probsChoiceN)
           
            inn = np.random.choice(randC,IN)

            wordO = self.train[c][window] # output Word

            contexts = []

            for x in range(0,window):

                contexts.append(self.train[c][x])

            mvec = np.mean(self.inVector[contexts],axis=0) # context

            # Begin Leaning #

            gradsCon =  -(self.alpha*self.gradient(mvec,self.outVector[wordO],d,mvec,wordO,len(contexts))[0])
            GradWord = self.gradient(mvec,self.outVector[wordO],d,mvec,wordO,len(contexts))[1]

            self.outVector[wordO] -= self.alpha*GradWord


            ###### Derivatives Negative Sampling
           
            for neg in range(0,negSampling):
               
                wordN = negs[neg]

                gradsCon +=  self.alpha*self.gradient(mvec,self.outVector[wordN],d,mvec,wordN,len(contexts))[0]
                GradWord = self.gradient(mvec,self.outVector[wordN],d,mvec,wordN,len(contexts))[1]

                self.outVector[wordN] += self.alpha*GradWord


            ###### Derivatives Importance Sampling

            if len(im) >= 1:

                Z,vecZ,probs,W = self.constZ(mvec,im,d)

                gradsCon += self.alpha*self.gradientIM(mvec,im,Z,vecZ,probs,len(contexts),d,W)[0]
                gradsWords = self.gradientIM(mvec,im,Z,vecZ,probs,len(contexts),d,W)[1]

                for index in range(0,Importance):

                    wordN = im[index]
                    self.outVector[wordN] += self.alpha*gradsWords[index]

            ################ Derivatives IN #################

            if len(inn) >= 1:

                vocabularyC = []

                for index in inn:

                    cc = []

                    for x in range(0,window):

                        cc.append(self.train[index][x])

                    mvec = np.mean(self.inVector[cc],axis=0)
                    vocabularyC.append(mvec)

                c,w = self.gradientIN(mvec,vocabularyC,len(cc),d,al,gama)
                self.outVector[wordO] -= self.alpha*w[wordO]

            #### Update inVector Context
            for n in contexts:

                if c in inn:

                    gradsCon -= self.alha*c[np.where(c == np.array(inn))[0][0]]
               
                self.inVector[n] += gradsCon



################################################
################################################
################################################

#Build matrix X(features)

def DiVeData(sentences,DiVe,sizeEmb,window,dist=0,test=0):

    X= []

    if test == 0:

        DiVe = DiVeWordEmbedding(dataset=sentences,size=args.size,window=args.win,d=dist,negSampling=args.negs,Importance=args.importance,IN=args.infrequent)

        for x in sentences:

            aux = np.zeros(sizeEmb)

            for word in x:

                if word in DiVe.vocabulary:

                        aux += DiVe.inVector[DiVe.vocabulary[word]]

            X.append(aux)
    else:

        for x in sentences:

            aux = np.zeros(sizeEmb)

            for word in x:

                if word in DiVe.vocabulary:

                        aux += DiVe.inVector[DiVe.vocabulary[word]]

            X.append(aux)
       
    return X,DiVe


sizeEmb = 400
window = 5
min_count = 1

from sklearn.model_selection import KFold

def mean_confidence_interval(data, confidence=0.95):
    a = 1.0*np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * sp.stats.t._ppf((1+confidence)/2., n-1)
    return m, m-(m-h), (m+h)-m

import sys
import argparse


if __name__ == '__main__':
   
    print "Hello World"

    parser = argparse.ArgumentParser()
    parser.add_argument('-size', help='Number dimension', dest='size', default=100, type=int)
    parser.add_argument('-window', help='Number of contexts', dest='win', default=3, type=int)
    parser.add_argument('-distances', help='Distances', dest='dist', default=0, type=int)
    parser.add_argument('-infrequent', help='Number of infrequent samples examples', dest='infrequent', default=0, type=int)
    parser.add_argument('-negative', help='Number of negative examples ', dest='negs', default=3, type=int)
    parser.add_argument('-importance', help='Number of importance samples examples', dest='importance', default=0, type=int)
    args = parser.parse_args()

    if args.importance != 0:
            args.negs = 0
    WordsEmbeddings = [DiVeData]
    keepEmb = [[]]

    stimators = [LogisticRegression(),SVC(),RandomForestClassifier(),GaussianNB(),MLPClassifier(),KNeighborsClassifier(),\
    LinearDiscriminantAnalysis(),QuadraticDiscriminantAnalysis()]

    metrics = [accuracy_score,f1_score,precision_score,recall_score]

    path = 'loads-Datas/'
    files =[['custrev.npy','label-custrev.npy'],["tweets.npy","labels-tweets.npy"],['polarity.npy','label-polarity.npy'],['question.npy','labels-question.npy'],["subj.npy","labels-subj.npy"],["imdb.npy",'labels-imdb.npy']]

    plots = 0

    for d in files:

        sentences = np.load(path+d[0])
        label = np.load(path+d[1])

        kf = KFold(n_splits=5,random_state=0)

        from sklearn.utils import shuffle
        sentences, label = shuffle(sentences,label,random_state=0)

        print "####DATA SET ## ####"
        print d
        for emb in range(0,len(WordsEmbeddings)):
           
            auxD = []
            auxE = []
            acc = []

            for train_index, test_index in kf.split(sentences):

                train = sentences[train_index]
                y = label[train_index]

                test = sentences[test_index]
                yt = label[test_index]
                
                X,keepEmb[emb] = WordsEmbeddings[emb](train,keepEmb[emb],args.size,args.win,dist=args.dist)

                Xn,keepEmb[emb] = WordsEmbeddings[emb](test,keepEmb[emb],sizeEmb,window,test=1)

                auxAcc = []
                for st in stimators:

                    st.fit(X,y)

                    pred = st.predict(Xn)

                    auxAcc.append(metrics[1](yt,pred,average='weighted'))

                acc.append(auxAcc)


            acc = np.array(acc)

            for row in range(0,len(acc[0])):

                m,t,ds = mean_confidence_interval(acc[:,row])
                auxD.append(m)
                auxE.append(t)

        print "Scores"
        for x,y,s in zip(auxD,auxE,stimators):
            print x,"(",y,"+/-)"#,s.get_params()
    