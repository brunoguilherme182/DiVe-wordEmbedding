import numpy as np
import pandas as pd
import random
import copy
import math

from scipy.spatial.distance import euclidean
from scipy.spatial.distance import cosine
from scipy.spatial.distance import chebyshev
from scipy.spatial.distance import cityblock
from scipy.spatial.distance import braycurtis
from scipy.spatial.distance import canberra

class DiVeWordEmbedding:

    N = 0

    def __init__(self,dataset,size=400,alpha=0.01,window=5,negSampling=2,threshold=-1,eta=0,d=0,W=[],Importance=3):

        """
        Main Class For build word embedding DiVe 
        """

        ### Vocabulary Variables 
        self.N = 0
        self.vocabulary = {}
        self.vocabularyCount = {}
        self.probsChoice = []
        self.constWeight = 0

        ### Train Variables 
        self.train = []
        self.alpha = alpha
        self.window = window

        self.distances = [euclidean,canberra,cityblock,euclidean,cosine]


        self.countWords(dataset)
        self.createVocabulary(dataset,window,threshold) 

        ### For Importance Sampling  and Negative Sampling
        self.createProbs(eta)

        self.LearningEmbedding(alpha,window,negSampling,size,eta,d,W,Importance)

    def countWords(self,dataset):

        '''
        
        Count Occurrence of Words in DataSet . Return Total Number of words apperrence 
        and total number apperrence of individual words 
        
        '''
        for line in dataset:

            for word in line:

                if word not in self.vocabularyCount:

                    self.vocabularyCount[word] = 0

                self.vocabularyCount[word] += 1

        for freqWord in self.vocabularyCount:

            self.N += self.vocabularyCount[freqWord]

    def createVocabulary(self,dataset,window,threshold):

        '''
        Create the vocabulary using for train the model, if necessary discard words with low Occurrence.
        Build the train set based  n-grams(window) and words not words not discarded 
        '''
        index = 0

        for line in dataset:

            sequence = []

            for word in line:

                probs = self.vocabularyCount[word]


                if probs < threshold:
                    continue

                if word not in self.vocabulary:
              
                    self.vocabulary[word] = index
                    index += 1

                sequence.append(self.vocabulary[word])

                if len(sequence) == window+1:

                   
                    sentence = []

                    for i in range(0,window+1):

                        sentence.append(sequence[i])

                    self.train.append(sentence)
                    sequence.pop(0)

    def createProbs(self,eta):

        '''
        Create a probability of apperrence of a word in dataset
        The probability will be use in negative Sampling and in Importance Sampling
        '''
        
        self.N = 0
        self.probsChoice = np.zeros(len(self.vocabulary))
        self.probsChoiceN = np.zeros(len(self.vocabulary))

        for freqWord in self.vocabularyCount:

            if freqWord in self.vocabulary:

                    self.N += self.vocabularyCount[freqWord]


        for word in self.vocabulary:

            self.probsChoice[self.vocabulary[word]] = ((self.vocabularyCount[word])/float(self.N))**(eta)
            self.constWeight += 1/float(self.probsChoice[self.vocabulary[word]])
            self.probsChoiceN[self.vocabulary[word]] = ((self.vocabularyCount[word])/float(self.N))**(1)
        
        norm = np.sum(self.probsChoice)
        self.probsChoice = self.probsChoice/float(norm)
        self.constWeight = self.constWeight**(-1)

    def constZ(self,mvec,vocabulary,d):


        dist = 0

        vecZ = []
        probs = []
        for word in vocabulary:
           
            dist = 0
                
            dist -= self.distances[d](self.inVector[word],mvec)
           
            vecZ.append(max(np.exp(dist),0.000000000000001))
            probs.append(self.probsChoice[word])

        return np.sum(vecZ),vecZ,probs


    def gradient(self,X,Y,d,context=-1,word=-1,divide=1.0):

        '''
        Calculate the first derivative of context and word with respect a some distance
        '''
        if (X==Y).all():

            return 0,0

        ### Euclidean Distance
        if d == 0:

            context = (X-Y)/self.distances[d](X,Y)*divide
            word = -(X-Y)/self.distances[d](X,Y)*divide

            return context,word

        ### Canberra Distance
        if d == 1:

            top = (X-Y)*(np.abs(Y)*np.abs(X)+X*Y)
            down = np.abs(X)*(np.abs(X)+np.abs(Y)**2)*np.abs(X-Y)

            topW = (Y-X)*(np.abs(Y)*np.abs(X)+X*Y)
            downW = np.abs(Y)*(np.abs(X)+np.abs(Y)**2)*np.abs(X-Y)

            return (top/down),(topW/downW)

        ### City Block Distance 
        if d == 2:

            context = (X-Y)/(np.abs(X-Y))
            word = (Y-X)/np.abs(X-Y)

            return  context,word
        
        #Need to build
        if d == 3:

            #print self.distances[d](X,Y),'dist'
            context = (X-Y)
            word = (Y-X)

            return  context,word


    def gradientIM(self,mvec,vocabulary,Z,vecZ,probs,divide,d):

        '''
        Calculate the first derivative of context and word with respect a some distance
        '''
        ### Euclidean Distance

        if d == 0:


            top = 0
            down = 0

            keepWords = [ np.zeros(len(mvec)) for x in range(0,len(vocabulary))]

            for index in range(0,len(vocabulary)):
                
                word = vocabulary[index]
                top -= ((mvec-self.outVector[word])*vecZ[index])/(divide*probs[index]*self.distances[d](mvec,self.outVector[word]))
                down +=  vecZ[index]/probs[index]

                keepWords[index] += ((mvec-self.outVector[word])*vecZ[index])/(probs[index]*self.distances[d](mvec,self.outVector[word]))

            return top/down,keepWords/down

        ### Need Build
        if d == 1:

            top = (X-Y)*(np.abs(Y)*np.abs(X)+X*Y)
            down = np.abs(X)*(np.abs(X)+np.abs(Y)**2)*np.abs(X-Y)

            topW = (Y-X)*(np.abs(Y)*np.abs(X)+X*Y)
            downW = np.abs(Y)*(np.abs(X)+np.abs(Y)**2)*np.abs(X-Y)

            return (top/down),(topW/downW)

        ### Need Build
        if d == 2:

            context = (X-Y)/(np.abs(X-Y))
            word = (Y-X)/np.abs(X-Y)

            return  context,word
        
        #Need to build
        if d == 3:

            top = 0
            down = 0

            keepWords = [ np.zeros(len(mvec)) for x in len(vocabulary)]
            for index in range(0,len(vocabulary)):
                
                word = vocabulary[index]
                top -= ((mvec-self.outVector[word])*vecZ[index])/(divide*probs[index])
                down +=  vecZ[index]/probs[index]

                keepWords[index] += ((mvec-self.outVector[word])*vecZ[index])/(probs[index])

            return top/down,keepWords*down**(-1)


    ############ Just Return the K most similar ###

    def mostSimilarI(self,w,d=0,k=25):

        word = self.vocabulary[w]
        dist = []
        for y in self.vocabulary:

                        x = self.vocabulary[y]
                        rt = self.distances[d](self.inVector[word],self.inVector[x])
                        dist.append([rt,y])
        cont = 0
        counts = []
        for x in sorted(dist):

                print x
                #counts.append(self.C[self.vocabulary[x[1]]])
                cont += 1
                if cont == k:
                        break
        #return counts


    def LearningEmbedding(self,alpha,window,negSampling,size,eta,d,W,Importance):

        '''
        Learning Dive Embedding 
        '''


        randC = len(self.train)
        randV = len(self.vocabulary)

        self.outVector = np.zeros([len(self.vocabulary),size])

        if len(W) == 0:

            if d == 2:
                self.alpha = np.zeros(len(self.vocabulary))+0.23
                self.inVector = np.random.uniform(low=-1,high=1,size=(len(self.vocabulary),size))
                self.outVector = self.inVector

            if d == 1:
                self.alpha = np.zeros(len(self.vocabulary))+0.45
                self.inVector = np.random.uniform(low=5,high=7,size=(len(self.vocabulary),size))
                self.outVector = self.inVector
                
            if d == 0 or d == 3:

                self.inVector = np.random.uniform(low=-0.5,high=0.5,size=(len(self.vocabulary),size))
        else:

            self.inVector = W

        for i in range(0,len(self.train)):

            c = i

            im = np.random.choice(randV,Importance,p=self.probsChoice)
            negs = np.random.choice(randV,negSampling,p=self.probsChoiceN)

            wordO = self.train[c][window]

            contexts = []

            for x in range(0,window):

                contexts.append(self.train[c][x])

            mvec = np.mean(self.inVector[contexts],axis=0)

            ######### Derivatives Context  and Word ##############

            gradsCon =  -(self.alpha*self.gradient(mvec,self.outVector[wordO],d,mvec,wordO,len(contexts))[0])

            GradWord = self.gradient(mvec,self.outVector[wordO],d,mvec,wordO,len(contexts))[1]

            self.outVector[wordO] -= self.alpha*GradWord


            ###### Derivatives Negative and Context 
            for neg in range(0,negSampling):


                wordN = negs[neg]

                gradsCon +=  self.alpha*self.gradient(mvec,self.outVector[wordN],d,mvec,wordN,len(contexts))[0]
                GradWord = self.gradient(mvec,self.outVector[wordN],d,mvec,wordN,len(contexts))[1]

                self.outVector[wordN] += self.alpha*GradWord


            ################ Derivatives Importance and Context #################

            if len(im) >= 1:

                Z,vecZ,probs = self.constZ(mvec,im,d)

                gradsCon += self.alpha*self.gradientIM(mvec,im,Z,vecZ,probs,len(contexts),d)[0]
                gradsWords = self.gradientIM(mvec,im,Z,vecZ,probs,len(contexts),d)[1]

                for index in range(0,Importance):

                    self.outVector[wordN] += self.alpha*gradsWords[index]


            #### Update inVector Context
            for n in contexts:

                self.inVector[n] += gradsCon

if __name__ == '__main__':
    
    print "Hello World"

    obj = DiVeWordEmbedding([['a','b'],['c','b'],['a','b'],['c','b'],['a','b'],['c','a'],['a','b'],['c','b'],['a','b']])
    obj.mostSimilarI('a')