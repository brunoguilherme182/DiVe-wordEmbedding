#fmerican Music Awards 2015rom sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.linear_model import LogisticRegression
import xgboost as xgb
from sklearn.metrics import accuracy_score
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import f1_score
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score

import numpy as np
from glove import Corpus, Glove
from gensim.models import Word2Vec
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.neighbors import KNeighborsClassifier


from scipy.spatial.distance import euclidean
from scipy.spatial.distance import cosine
from scipy.spatial.distance import chebyshev
from scipy.spatial.distance import cityblock
from scipy.spatial.distance import braycurtis
from scipy.spatial.distance import canberra



#from cleanText import treatText

import numpy as np
import scipy as sp
import scipy.stats
#from LMEg4 import LMEwordEmbedding
#from ds import DSWEwordEmbedding
from DiVeWordEmbedding import DiVeWordEmbedding as DSWEwordEmbedding
#from cleanText import treatText
#from TWEexactN import ELMEwordEmbedding

   
sizeEmb = 400
window = 5
min_count = 1

def mean_confidence_interval(data, confidence=0.95):
    a = 1.0*np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * sp.stats.t._ppf((1+confidence)/2., n-1)
    return m, m-(m-h), (m+h)-m

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
count_vect = CountVectorizer()
tf_transformer = TfidfTransformer(use_idf=False)


if __name__ == '__main__':
   
    print "Hello World"
    

    path = 'loads-Datas/'

    f =["tweets.npy",'labels-imdb.npy']

    print "##### SMALL DATASETS ##########"

    sentences = np.load(path+f[0])

    LME = DSWEwordEmbedding(dataset=sentences,size=sizeEmb,window=window)

    print 'inVector'
    print LME.mostSimilarI("best",d=0)
    print LME.mostSimilarI("good",d=0)
    print LME.mostSimilarI("islam",d=0)
    print LME.mostSimilarI("isis",d=0)
    print LME.mostSimilarI("man",d=0)
    print LME.mostSimilarI("woman",d=0)
    print 'inVector Cosine'
    print LME.mostSimilarI("best",d=4)
    print LME.mostSimilarI("good",d=4)
    print LME.mostSimilarI("islam",d=4)
    print LME.mostSimilarI("isis",d=4)
    print LME.mostSimilarI("man",d=4)
    print LME.mostSimilarI("woman",d=4)

