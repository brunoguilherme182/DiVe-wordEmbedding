
import numpy as np
import scipy as sp
import scipy.stats

from sklearn.manifold import MDS
from sklearn.manifold import Isomap
from sklearn.manifold import LocallyLinearEmbedding
from sklearn.manifold import SpectralEmbedding

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer

from sklearn.metrics.pairwise import cosine_similarity
from scipy.spatial.distance import cosine

def buildMatrixCosine(train):

    count_model = CountVectorizer(ngram_range=(1,1))
    X = count_model.fit_transform(train)
    Xc = (X.T * X) # this is co-occurrence matrix in sparse csr format
    #print(Xc.todense())

    similarities = cosine_similarity(Xc)

    return similarities,count_model.vocabulary_


def buildMatrixTFIDF(train):

    count_model = CountVectorizer(ngram_range=(1,1))
    X = count_model.fit_transform(train)
    Xc = (X.T * X) # this is co-occurrence matrix in sparse csr format
    #print(Xc.todense())

    transformer = TfidfTransformer()
    transformed_weights = transformer.fit_transform(Xc)
    
    return transformed_weights.toarray(),count_model.vocabulary_


def buildMDS(data,sentences,mds,vocabulary,size=400,test=0):

    X = []

    if test == 0:

        mds = MDS(n_components=size)
        wordsEmb = mds.fit_transform(data)

        for line in sentences:

            aux = np.zeros(size)

            for word in line:

                aux += wordsEmb[vocabulary[word]]

            X.append(aux)

    else:

        for line in sentences:

            aux = np.zeros(size)

        for word in line:

            if word in vocabulary:
                
                aux += wordsEmb[vocabulary[word]]

        X.append(aux)

    return X,mds


def buildIsomap(data,sentence,mds,size,vocabulary):

    X = []

    if test == 0:

        mds = Isomap(n_components=size)
        wordsEmb = mds.fit_transform(data)

        for line in sentences:

            aux = np.zeros(size)

            for word in line:

                aux += wordsEmb[vocabulary[word]]

            X.append(aux)

    else:

        for line in sentences:

            aux = np.zeros(size)

        for word in line:

            if word in vocabulary:
                
                aux += wordsEmb[vocabulary[word]]

        X.append(aux)

    return X,mds

if __name__ == '__main__':
    
    print "Hello World"


