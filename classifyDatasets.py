#import xgboost as xgb
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.neighbors import KNeighborsClassifier

from sklearn.metrics import f1_score
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score

from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score

from glove import Corpus, Glove
from gensim.models import Word2Vec


import numpy as np
import scipy as sp
import scipy.stats

from buildManifold import buildMDS
from buildManifold import buildIsomap
from buildManifold import buildMatrixCosine

from buildWordEmb import DiVeData
from buildWordEmb import word2vecData
from buildWordEmb import gloveData
from buildWordEmb import FastText
from buildWordEmb import buildBayesianSG


from barplotAcc import buildBarplot
import copy
sizeEmb = 400
window = 5
min_count = 1

from sklearn.model_selection import KFold

def mean_confidence_interval(data, confidence=0.95):
    a = 1.0*np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * sp.stats.t._ppf((1+confidence)/2., n-1)
    return m, m-(m-h), (m+h)-m

if __name__ == '__main__':

    print "Hello World"


   
    WordsEmbeddings = [word2vecData,gloveData,buildBayesianSG,FastText,DiVeData]

    saves = ['Word2Vec','GloVe','Bayesian Skip Gram','FastText','DiVe']

    keepEmb = [[],[],[],[],[]]

    manifold = [buildMDS,buildIsomap]

    stimators = [LogisticRegression(),SVC(),RandomForestClassifier(),GaussianNB(),MLPClassifier(),KNeighborsClassifier(),\
    LinearDiscriminantAnalysis(),QuadraticDiscriminantAnalysis()]

    metrics = [accuracy_score,f1_score,precision_score,recall_score]

    path = 'loads-Datas/'
    files =[['custrev.npy','label-custrev.npy'],["tweets.npy","labels-tweets.npy"],['polarity.npy','label-polarity.npy'],['question.npy','labels-question.npy'],["subj.npy","labels-subj.npy"],["imdb.npy",'labels-imdb.npy']]

    names ='plotsDSWEDualEu'
    plots = 0

    data = []
    erro = []

    for d in files:

        sentences = np.load(path+d[0])
        label = np.load(path+d[1])

        kf = KFold(n_splits=5,random_state=0)

        from sklearn.utils import shuffle
        sentences, label = shuffle(sentences,label,random_state=0)

        data = []
        erro = []
        print "####DATA SET"
        print d
        print "###########"
        for emb in range(0,len(WordsEmbeddings)):
            
            auxD = []
            auxE = []
            acc = []
            sl = 0
            for train_index, test_index in kf.split(sentences):

                train = sentences[train_index]
                y = label[train_index]

                test = sentences[test_index]
                yt = label[test_index]
                
                X,keepEmb[emb] = WordsEmbeddings[emb](train,keepEmb[emb],sizeEmb,window)
                #np.save(saves[emb]+'TrainCan'+str(sl)+d[0],X)
                
                auxAcc = []

                Xn,keepEmb[emb] = WordsEmbeddings[emb](test,keepEmb[emb],sizeEmb,window,test=1)
                #np.save(saves[emb]+'TesteCan'+str(sl)+d[0],Xn)
                
                sl += 1
                
                for st in stimators:

                    st.fit(X,y)

                    pred = st.predict(Xn)

                    auxAcc.append(metrics[1](yt,pred,average='weighted'))

                acc.append(auxAcc)


            acc = np.array(acc)

            for row in range(0,len(acc[0])):

                m,t,ds = mean_confidence_interval(acc[:,row])
                auxD.append(m)
                auxE.append(t)


            data.append(auxD)
            erro.append(auxE)
        del X
        del y
        print data
        print erro
        buildBarplot(data,erro,names+str(plots)+".png")
        plots += 1




      
